describe('Reading entry file', () => {
    it('Reads the entry file typed by the user', () => {
        cy.visit('http://localhost:3000');

        //On tape le fichier d'entrée
        cy.get('[data-testid="entryFileText"]')
          .type('C - 3 - 4\nM - 1 - 0\nM - 2 - 1\nT - 0 - 3 - 2\nT - 1 - 3 - 3\nA - Lara - 1 - 1 - S - AADADAGGA');

        //On lance le jeu 
        cy.get('[data-testid="startBtn"]')
          .click();

        //On vérifie qu'on est bien redirigé sur la page de jeu
        cy.url().should('include', '/game')

        //on vérifie que le rappel du ficher d'entrée correspond bien à ce que l'on a tappé
        cy.get('[data-testid="entryFileText"]')
          .should('have.value', 'C - 3 - 4\nM - 1 - 0\nM - 2 - 1\nT - 0 - 3 - 2\nT - 1 - 3 - 3\nA - Lara - 1 - 1 - S - AADADAGGA');

        //On vérifie au passage que la position et l'orientation de l'aventurier est correcte
        cy.get('[id="1-1"]').get('img')
          .should('have.class', 'map-icon')
          .and('have.class', 'facing-south')
        
        //On joue jusqu'à ce qu'il n'y ait plus de mouvements à faire
        for(let i = 0; i<9; i++){
          cy.get('[data-testid="nextStepBtn"]')
            .click();
        }
        
        //On vérifie que le fichier de sortie est généré correctement
        cy.get('[data-testid="outpuyFileText"]')
          .should('have.value', 'C - 3 - 4\nM - 1 - 0\nM - 2 - 1\n# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}\nT - 1 - 3 - 2\n# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}\nA - Lara - 0 - 3 - S - 3');

        
    })
})