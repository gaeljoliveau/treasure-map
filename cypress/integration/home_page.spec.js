

describe('Visit home page', () => {
    it('Shows the guidelines to the user', () => {
        cy.visit('http://localhost:3000');

        cy.get('[data-testid="entryFileLbl"]')
            .should('contain', 'Fichier d\'entrée :');

        cy.get('[data-testid="entryFileText"]')
            .should('have.value', '');

        cy.get('[data-testid="startBtn"]')
            .should('contain', 'Commencer');
    })
})