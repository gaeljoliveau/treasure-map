import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";

//Pages 
import Home from "./pages/Home.js";
import Game from './pages/Game.js';

const App = () => {
  return (
    <BrowserRouter>
      <Route path="/" exact component={Home}/>
      <Route path="/game" component={Game} />
    </BrowserRouter>
  );
};

export default App;
