import React, { useState, useEffect} from 'react';
import { useLocation } from "react-router-dom";
import mountain from '../assets/svg/mountain.svg';
import treasure from '../assets/svg/treasure.svg';
import adventurer from '../assets/svg/adventurer.svg';

const Game = () => {
    const [mapInfo, setMapInfo] = useState([]);
    const [adventurers, setAdventurers] = useState([]);
    const [turn, setTurn] = useState(0);
    const [entryFileString, setEntryFileString] = useState('');
    const [gameLog, setGameLog] = useState('');
    const location = useLocation();

    useEffect(() => {
        if(location.state.entryFile.mapInfo.length > 0){
            //On instancie les variables du state
            setMapInfo(location.state.entryFile.mapInfo)
            setAdventurers(location.state.entryFile.adventurers)
            setTurn(0);
            setEntryFileString(location.state.entryFileString)

            //On prépare les logs de début de partie
            let initLog = `Construction d'une carte de ${location.state.entryFile.mapInfo.length} lignes et ${location.state.entryFile.mapInfo[0].length} colonnes \n`
            //On mets un message si un des aventuriers apparaît sur une case trésor
            location.state.entryFile.adventurers.forEach(adventurer => {
                if(adventurer.isOnTreasure){
                    initLog+=`${adventurer.name} est apparu sur une case trésor, quelle chance !\n`
                }
            });

            setGameLog(initLog)
        }else{
            setGameLog("Fichier d'entrée vide, construction du jeu impossible \n")
            setTurn(-1)
        }
        
    },[location]);

    const nextStep = () => {
        //Déplacer chaque aventurier sur la carte selon le movement définit
        let newMapInfo = mapInfo;
        //On mets à jour le fichier des aventurier en prennant en compte le mouvement effectué par chacun
        let adventurersHaveFinishedMoving = 0
        let newGameLog = gameLog;
        setAdventurers(adventurers.map(adventurer => {
            //Switch en fonction du mouvement à effectuer : A pour Avancer, D pour tourner à droite, G pour tourner à Gauche
            switch (adventurer.moves.charAt(turn)) {
                case 'A':
                    let oldX = parseInt(adventurer.pos.split('-')[0]);
                    let oldY = parseInt(adventurer.pos.split('-')[1]);
                    let newX = oldX;
                    let newY = oldY;
                    //Calcul de la nouvelle position
                    switch (adventurer.orientation) {
                        case 'N':
                            newY--;
                            break;
                        case 'E':
                            newX++;
                            break;
                        case 'S':
                            newY++;
                            break;
                        case 'O':
                            newX--;
                            break;
                        default:
                            break;
                    }
                    
                    if(newX > mapInfo[1].length-1 || newY > mapInfo.length-1 || newX < 0 || newY < 0 ){
                        newGameLog+= adventurer.name + " ne peut pas avancer sur la case " + newX +"-" + newY + " car il sortirait de la carte\n"
                    }else{
                        switch (newMapInfo[newY][newX].type) {
                            //L'aventurier avance sur une case vide
                            case "empty":
                                newGameLog+= adventurer.name + " avance sur la case " + newX +"-" + newY + '\n'
                                adventurer.pos = newX + '-' + newY;
                                //Si l'aventurier se trouvait sur une cas sur laquelle il y avait un trésor
                                if(newMapInfo[oldY][oldX].isOnTreasure){
                                    //On déplace l'aventurier sur la nouvelle case
                                    newMapInfo[newY][newX] = newMapInfo[oldY][oldX]
                                    newMapInfo[newY][newX].pos = adventurer.pos
                                    //On dit qu'il ne se trouve plus sur une case trésor
                                    newMapInfo[newY][newX].isOnTreasure = false
                                    
                                    //On met à jour l'ancienne case
                                    if(newMapInfo[newY][newX].treasureInfo.count > 0 ){
                                        //si il reste des trésor à découvrir on replace les trésors restants
                                        newMapInfo[oldY][oldX] = newMapInfo[newY][newX].treasureInfo 
                                    }else{
                                        //si il ne reste plus de trésors à découvrir la case devient vide
                                        newMapInfo[oldY][oldX]={type: "empty"}
                                    }
                                    //On supprime les infos du trésor car l'aventurier se trouve désormais sur une case vide
                                    delete newMapInfo[newY][newX].treasureInfo 
    
                                }else{ //Dans les autres cas il se trouvait sur une case sur laquelle il n'y a pas de trésor
                                    newMapInfo[newY][newX] = newMapInfo[oldY][oldX];
                                    newMapInfo[newY][newX].pos = adventurer.pos
                                    newMapInfo[oldY][oldX] = {
                                        type: "empty"
                                    }
                                }
                                break;

                            //L'aventurier essaie d'avancer sur une case montagne
                            case "mountain":
                                newGameLog+= adventurer.name + " ne peut pas avancer sur la case " + newX +"-" + newY + " car il y a une montagne\n "
                                break;
                            
                            //L'aventurier avance sur une case trésor
                            case "treasure":
                                newGameLog+=adventurer.name + " avance sur la case " + newX +"-" + newY + " et y découvre un trésor\n"
                                adventurer.pos = newX + '-' + newY;
                                //Si l'aventurier se trouvait sur une cas sur laquelle il y avait déjà un trésor
                                if(newMapInfo[oldY][oldX].isOnTreasure){

                                    //On copie les infos du trésor et on diminue son nombre
                                    let treasure = newMapInfo[newY][newX]
                                    treasure.count--

                                    //On déplace l'aventurier et on ajoute 1 au nombre de trésor qu'il à trouvé
                                    newMapInfo[newY][newX] = newMapInfo[oldY][oldX]
                                    newMapInfo[newY][newX].treasureCount++
                                    adventurer.treasureCount++

                                    //On remets l'ancien trésor à sa place
                                    newMapInfo[oldY][oldX] = newMapInfo[newY][newX].treasureInfo

                                    //Si il reste des trésors à découvrir, on copie les informations du nouveau trésor pour être capable de le replacer au prochain tour
                                    if(treasure.count > 0){
                                        newMapInfo[newY][newX].treasureInfo = treasure
                                    }else{ // Il vient de découvrir le dernier trésor sur cette case
                                        //On supprime les information de l'ancien trésor et on indique qu'il ne se trouve plus sur une case trésor
                                        delete newMapInfo[newY][newX].treasureInfo
                                        newMapInfo[oldY][oldX].isOnTreasure = false
                                    }
                                    
                                    
                                }else{ //Dans les autres cas il se trouvait sur une case sur laquelle il n'y a pas de trésor

                                    //On copie les infos du trésor et on diminue son nombre
                                    let treasure = newMapInfo[newY][newX]
                                    treasure.count--

                                    //On déplace l'aventurier et on ajoute 1 au nombre de trésor qu'il à trouvé
                                    newMapInfo[newY][newX] = newMapInfo[oldY][oldX]
                                    newMapInfo[newY][newX].treasureCount++
                                    adventurer.treasureCount++

                                    //Si il reste des trésors à découvrir on dit qu'il se trouve sur une case trésor et on copie les informations du trésor pour être capable de le replacer au prochain tour
                                    if(treasure.count > 0){
                                        newMapInfo[newY][newX].isOnTreasure = true
                                        newMapInfo[newY][newX].treasureInfo = treasure
                                    }
                                    //On remets l'ancienne case à vide
                                    newMapInfo[oldY][oldX] = {
                                        type: "empty"
                                    }
                                }
                                break;
                            //L'aventurier essaie d'avancer sur une case où un autre aventurier se trouve déjà
                            case "adventurer":
                                newGameLog+= adventurer.name + " ne peut pas avancer sur la case " + newX +"-" + newY + " car il y a déja un aventurier\n"
                                break;
                            //L'aventurier essaie d'avancer sur une case où un autre aventurier se trouve déjà et où il y a un trésor
                            case "adventurer&treasure":
                                newGameLog+=adventurer.name + " ne peut pas avancer sur la case " + newX +"-" + newY + " car il y a déja un aventurier\n"
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 'G':
                    newGameLog+=`${adventurer.name} tourne à gauche\n`;
                    switch (adventurer.orientation) {
                        case 'N':
                            //On tourne l'aventurier vers la gauche dans le fichier des aventuriers 
                            adventurer.orientation='O'
                            //Et on met à jour la carte avec la nouvelle position de l'aventurier
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='O'
                            break;
                        case 'E':
                            adventurer.orientation='N'
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='N'
                            break;
                        case 'S':
                            adventurer.orientation='E'
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='E'
                            break;
                        case 'O':
                            adventurer.orientation='S'
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='S'
                            break;
                        default:
                            break;
                    }
                    break;
                case 'D':
                    newGameLog+=`${adventurer.name} tourne à droite\n`;
                    switch (adventurer.orientation) {
                        case 'N':
                            //On tourne l'aventurier vers la droite dans le fichier des aventuriers 
                            adventurer.orientation='E'
                            //Et on met à jour la carte avec la nouvelle position de l'aventurier
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='E'
                            break;
                        case 'E':
                            adventurer.orientation='S'
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='S'
                            break;
                        case 'S':
                            adventurer.orientation='O'
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='O'
                            break;
                        case 'O':
                            adventurer.orientation='N'
                            newMapInfo[adventurer.pos.split('-')[1]][adventurer.pos.split('-')[0]].orientation='N'
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            //Si on vient d'effectuer le dernier mouvement
            if(turn === adventurer.moves.length-1){
                newGameLog+="Plus de mouvements pour l'aventurier " + adventurer.name + '\n'
                adventurersHaveFinishedMoving++;
            }
            return adventurer;
        }))


        //Si tous les aventuriers ont terminé leurs déplacements, on déclare la fin du jeu
        if(adventurersHaveFinishedMoving === adventurers.length){
            setTurn(-1);
            newGameLog+= "Tous les aventuriers on terminés leurs déplacements\n"
            
        }else{
            setTurn(turn+1);
            setMapInfo(newMapInfo);
        }
        setGameLog(newGameLog);
    }

    const writeOutputFile = () =>{
        //On initialise des variables pour chaque type d'information afin de pouvoir places des commentaires dans le fichier de sortie
        let mountains = "";
        let treasures = "";
        let adventurers = "";

        //Ensuite on ajoute les informations sur le placement des montagnes
        mapInfo.forEach((line, lineIndex)=>{
            line.forEach((element, colIndex) => {
                switch (element.type) {
                    case "mountain":
                        mountains += `M - ${colIndex} - ${lineIndex}\n`
                        break;
                    case "treasure":
                        treasures += `T - ${colIndex} - ${lineIndex} - ${element.count}\n`
                        break;
                    case "adventurer":
                        adventurers += `A - ${element.name} - ${colIndex} - ${lineIndex} - ${element.orientation} - ${element.treasureCount}`
                        if(element.isOnTreasure){
                            treasures += `T - ${colIndex} - ${lineIndex} - ${element.treasureInfo.count}\n`
                        }
                        break;
                    default:
                        break;
                }
            })
        })

        return(
            `C - ${mapInfo[0].length} - ${mapInfo.length}\n` 
            + mountains
            + "# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}\n"
            + treasures
            + "# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}\n"
            + adventurers);
            
    }


    return(
        <div className="page-container">
            <div  className="main-content">
                <h1 className="text">La carte aux trésors : Jeu</h1>  
                <p className="text">Rappel du fichier d'entrée :</p>
                <textarea data-testid="entryFileText" id='entryFileText' className='entry-file-area' value={entryFileString} readOnly/>

                <p className="text">Déroulement du jeu :</p>
                <textarea data-testid="logText" id='logText' className='entry-file-area' value={gameLog} readOnly/>

                <p className="text">Carte :</p>
                <table className="map">
                    <tbody>
                        {
                            mapInfo.map((line, i) => 
                                <tr className='map-line' id={"line " +i} key={"line " +i}> 
                                    {
                                        line.map((element, j) => <td className="map-element" id={i + "-" + j} key={i + "-" + j}>{
                                            element.type ==='mountain'? 
                                                <img src={mountain} alt="Montagne" className="map-icon"/> : 
                                            element.type=== 'treasure'? 
                                                <div className="map-treasure-container">
                                                    <img src={treasure} alt="Trésor" className="map-icon" />
                                                    <p className="map-treasure-count text">{element.count}</p>
                                                </div> :
                                            element.type === 'adventurer'?
                                                <img src={adventurer} alt="Aventurier" className={`map-icon facing-${element.orientation === 'E'? 'east': 
                                                                                                                    element.orientation === 'S'? 'south':
                                                                                                                    element.orientation === 'O'? 'west':
                                                                                                                    element.orientation === 'N'? 'north':
                                                                                                                    ''}`}/> : 
                                            '' }</td>)
                                    }
                                </tr>
                            )   
                        }
                    </tbody>
                </table>
                <br/>
                <button data-testid="nextStepBtn" id='nextStepBtn' onClick={turn === -1 ? () => setGameLog(gameLog+ "Jeu terminé \n") : nextStep} className="btn">{turn === -1? "Jeu terminé" : "Etape suivante"}</button>

                {
                    turn === -1 ? 
                        <div>
                            <p className="text">Fichier de sortie :</p>
                            <textarea data-testid="outpuyFileText" id='outpuyFileText' className='entry-file-area' value={writeOutputFile()} readOnly/>
                        </div>
                    : "" 
                }

            </div>
        </div>
    )
}

export default Game;