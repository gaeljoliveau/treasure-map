import React, { useState } from 'react';
import { useHistory  } from "react-router-dom";
import '../assets/css/app.css';

const Home = () => {
    const [entryFile, setEntryFile] = useState('')
    const [errorMessage, setErrorMessage] = useState('')
    const history = useHistory()

    const startGame = (e) => {
        //On lance la partie seulement si le fichier d'entrée est valide
        if (checkEntryFile()){
            e.preventDefault();
            history.push("/game", { 'entryFileString': entryFile, 'entryFile':readEntryFile()});
        }
        
    }

    const checkEntryFile = () => {
        //Vérification que le fichier d'entrée n'est pas vide
        if(entryFile.length === 0){
            setErrorMessage("Le fichier d'entrée est vide, veuillez renseigner le fichier d'entrée");
            return false;
        }else{

            let entryFileArray = entryFile.split('\n');
            //On instancie ses bouléens afin de pouvoir lever une erreur si le fichier est incorrect
            let haveAMapLine = false;
            let haveIncorrectLines = false;
            //On instancie des tableaux afin de pouvoir vérifier si les aventuriers et trésors ne sont pas placer sur des montagnes 
            let adventurersPositions = [];
            let mountainsPositions = [];
            let treasuresPositions = [];
            let mapDimensions = ''

            //On parcours les lignes du fichier d'entrée afin de vérifier si on ne trouves pas d'erreurs
            entryFileArray.forEach(line => {
                switch (line.substring(0,1).toUpperCase()) {
                    case 'C':
                        //On a bien un ligne qui définit une carte car on vient de rentrer dans le cas ou la ligne commence par un C
                        haveAMapLine = true;
                        //On verifie qu'on a bien le bon nombre de champs
                        if(line.split('-').length !== 3){
                            haveIncorrectLines = true
                            setErrorMessage("La ligne de carte est incorrecte :\n{C comme Carte} - {Nb. de case en largeur} - {Nb. de case en hauteur}")
                        }
                        //On vérifie que les informations passés pour la taille sont bien des entiers
                        else if(!Number.isInteger(parseInt(line.split('-')[1].trim())) || !Number.isInteger(parseInt(line.split('-')[2].trim())) ){ 
                            haveIncorrectLines = true;
                            setErrorMessage("La ligne de carte est incorrecte, le nombre de case doit être un nombre entier:\n{C comme Carte} - {Nb. de case en largeur} - {Nb. de case en hauteur}")
                        }else{
                            //On stocke la dimension de la carte afin de pouvoir vérifier si certains éléments n'ont pas été placés en dehhors de la carte 
                            mapDimensions = `${line.split('-')[1].trim()}-${line.split('-')[2].trim()}`
                        }
                        break;

                    case 'M':
                        //On verifie qu'on a bien le bon nombre de champs pour une montagne
                        if(line.split('-').length !== 3){
                            haveIncorrectLines = true
                            setErrorMessage("Une des lignes de montagnes est incorrecte :\n{M comme Montagne} - {Axe horizontal} - {Axe vertical}")
                        }
                        //On vérifie que les informations passés pour la position sont bien des entiers
                        else if(!Number.isInteger(parseInt(line.split('-')[1].trim())) || !Number.isInteger(parseInt(line.split('-')[2].trim())) ){ 
                            haveIncorrectLines = true;
                            setErrorMessage("Une des lignes de montagnes est incorrecte, la position doit être un nombre entier :\n{M comme Montagne} - {Axe horizontal} - {Axe vertical}")
                        }else{
                            //La ligne est correcte mais on stocke sa position que l'utilisateur ne veut pas placer un aventurier ou un trésor sur la même case
                            mountainsPositions.push(`${line.split('-')[1].trim()}-${line.split('-')[2].trim()}`)
                        }
                        break;

                    case 'T':
                        //On verifie qu'on a bien le bon nombre de champs pour un trésor
                        if(line.split('-').length !== 4){
                            haveIncorrectLines = true
                            setErrorMessage("Une des lignes de trésors est incorrecte :\n{T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors}")
                        }
                        //On vérifie que les informations passés pour la taille & le nombre de trésors sont bien des entiers 
                        else if(!Number.isInteger(parseInt(line.split('-')[1].trim())) || !Number.isInteger(parseInt(line.split('-')[2].trim())) || !Number.isInteger(parseInt(line.split('-')[3].trim())) ){ 
                            haveIncorrectLines = true;
                            setErrorMessage("La ligne de carte est incorrecte, la position et le nombre de trésors doivent être des nombres entiers :\n{T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors}")
                        }else{
                            //La ligne semble correcte mais on stocke sa position pour vérifier qu'il n'apparait pas sur une montagne
                            treasuresPositions.push(`${line.split('-')[1].trim()}-${line.split('-')[2].trim()}`)
                        }
                        break;

                    case 'A':
                        //On verifie qu'on a bien le bon nombre de champs pour un aventurier
                        if(line.split('-').length !== 6){
                            haveIncorrectLines = true
                            setErrorMessage("Une des lignes d'aventurier est incorrecte :\n{A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}")
                        }
                        //On vérifie que les informations passés pour la position sont bien des entiers
                        else if(!Number.isInteger(parseInt(line.split('-')[2].trim())) || !Number.isInteger(parseInt(line.split('-')[3].trim())) ){
                            haveIncorrectLines = true;
                            setErrorMessage("Une des lignes d'aventurier est incorrecte, l'axe horizontal et vertical doivent être des nombres entiers :\n{A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}")
                        }
                        //On vérifie que l'orientation de l'aventurier est bien soit N, soit S, soit E, soit O
                        else if( !['N', 'S', 'E', 'O'].includes(line.split('-')[4].trim().toUpperCase()) ){
                            haveIncorrectLines = true;
                            setErrorMessage("Une des lignes d'aventurier est incorrecte, l'orientation doit-être une des lettres suivantes, N, S, E, O :\n{A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}")
                        }
                        //On vérifie que la séquence de mouvement n'est composée que de A, D et G
                        else if(line.split('-')[5].trim().toUpperCase().match(/^[ADG]+$/g) === null ){
                            haveIncorrectLines = true;
                            setErrorMessage("Une des lignes d'aventurier est incorrecte, la séquence de mouvement ne peut être composée que des lettres suivantes, A, D, G :\n{A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}")
                        }else{
                            //La ligne semble correcte mais on stocke sa position pour vérifier qu'il n'apparait pas sur une montagne
                            adventurersPositions.push(`${line.split('-')[2].trim()}-${line.split('-')[3].trim()}`)
                        }
                        break;
                    case '#':
                        break;

                    default:
                        //La ligne est non-interpretable par le programme, on remonte l'erreur à l'utilisateur
                        haveIncorrectLines = true
                        setErrorMessage("Ligne erronnée, il existe seulement 5 types de lignes possible : \n Les cartes commencent par C\nLes montagnes commencent par M\nLes trésors commencent par T\nLes Aventuriers commencent par A\nles commentaires commencent par #")
                        break;
                }
            });

            //Si on a pas déjà une ligne incorrecte
            if(!haveIncorrectLines){
                //On parcours les positions des montagnes
                mountainsPositions.forEach(mountainPosition => {
                    //On vérifie que la montagne n'est pas placée en dehors de la carte
                    if(mountainPosition.split('-')[0] < 0 || mountainPosition.split('-')[1] < 0 || mountainPosition.split('-')[0] > mapDimensions.split('-')[0]-1 || mountainPosition.split('-')[1] > mapDimensions.split('-')[1]-1){
                        haveIncorrectLines = true
                        setErrorMessage("Vous essayez de positionner une montagne hors de la carte")
                    }
                    //On vérifie qu'aucun aventurier n'est pas placé sur la montagne
                    else if(adventurersPositions.includes(mountainPosition)){
                        haveIncorrectLines = true
                        setErrorMessage("Vous essayez de positionner un aventurier sur une montagne, les aventurier ne sont pas montagnards pour un sou")
                    }
                    //On vérifie qu'aucun trésor n'est pas placé sur la montagne
                    else if(treasuresPositions.includes(mountainPosition)){
                        haveIncorrectLines = true
                        setErrorMessage("Vous essayez de positionner un trésor sur une montagne, veuillez déplacer le trésor ou la montagne")
                    }
                })

                //On vérifie qu'aucun trésor ne se trouve en dehors de la carte
                treasuresPositions.forEach(treasurePosition => {
                    if(treasurePosition.split('-')[0] < 0 || treasurePosition.split('-')[1] < 0 || treasurePosition.split('-')[0] > mapDimensions.split('-')[0]-1 || treasurePosition.split('-')[1] > mapDimensions.split('-')[1]-1){
                        haveIncorrectLines = true
                        setErrorMessage("Vous essayez de positionner un trésor hors de la carte")
                    }
                })

                //On vérifie qu'aucun aventurier ne se trouve en dehors de la carte
                adventurersPositions.forEach(adventurerPosition => {
                    if(adventurerPosition.split('-')[0] < 0 || adventurerPosition.split('-')[1] < 0 || adventurerPosition.split('-')[0] > mapDimensions.split('-')[0]-1 || adventurerPosition.split('-')[1] > mapDimensions.split('-')[1]-1){
                        haveIncorrectLines = true
                        setErrorMessage("Vous essayez de positionner un aventurier hors de la carte")
                    }
                })

            }
            
            

            if (haveAMapLine && !haveIncorrectLines){
                return true
            }else{
                return false
            }
        }
        
    }

    const readEntryFile = () => {

        let entryFileArray = entryFile.split('\n');

        //Cration de l'objet qui sera retourné par la fonction
        let entryfileOject = {
            mapInfo:[],
            adventurers:[]
        }

        //Tri du taleau pour avoir la taille de ma carte en premier, puis les montagnes, puis les trésors
        entryFileArray.sort((a, b) => {
            switch (a.substring(0,1).toUpperCase()) {
                case 'C':
                    //C toujours en priorité
                    return -1;
                case 'M':
                    //M toujours en priorité sauf si on le compare à une carte
                    if(b.substring(0,1) === 'C'){
                        return 1
                    }
                    return -1

                case 'T':
                    //M toujours en priorité sauf si on le compare à une carte ou une montagne
                    if(b.substring(0,1) === 'C' || b.substring(0,1) === 'M'){
                        return 1
                    }
                    return -1
                case 'A':
                    //A toujours en priorité sauf si on le compare à une carte ou une montagne, un trésor ou un aventurier
                    if(b.substring(0,1) === 'C' || b.substring(0,1) === 'M' || b.substring(0,1) === 'T'|| b.substring(0,1) === 'A'){
                        return 1
                    }
                    return -1
                default:
                    //autre cas pas prioritaire
                    return -1
            }
        })

        entryFileArray.forEach((line)=>{
            switch (line.substring(0,1).toUpperCase()) {
                //on construit d'abord le tableau avec toutes les cases vides grâce aux dimensions de la carte 
                case 'C':
                    for(let i = 0; i<line.split('-')[2].trim(); i++){
                        let mapLine = [];
                        for(let j = 0; j<line.split('-')[1].trim(); j++){
                            mapLine.push({type:'empty'});
                        }
                        entryfileOject.mapInfo.push(mapLine)
                    }
                    break;
                // et ensuite on place les éléments
                case 'M':
                    entryfileOject.mapInfo[line.split('-')[2].trim()][line.split('-')[1].trim()] = {type:'mountain'}
                    break;
                case 'T':
                    entryfileOject.mapInfo[line.split('-')[2].trim()][line.split('-')[1].trim()] = {type: 'treasure', count: parseInt(line.split('-')[3].trim())}
                    break;
                case 'A':
                    //On instancie les variables afin de pouvoir stocker les informations du trésor si l'aventurier apparaît sur un trésor
                    let isOnTreasure = false;
                    let treasureCount = 0;
                    let treasureInfo = {}
                    //On vérifie si il apparaît sur un trésor
                    if(entryfileOject.mapInfo[line.split('-')[3].trim()][line.split('-')[2].trim()].type === 'treasure'){
                        isOnTreasure = true;
                        treasureCount++;
                        treasureInfo = entryfileOject.mapInfo[line.split('-')[3].trim()][line.split('-')[2].trim()];
                        treasureInfo.count--;
                    }

                    //On place l'aventuruer sur la carte d
                    entryfileOject.mapInfo[line.split('-')[3].trim()][line.split('-')[2].trim()] =
                        {
                            type: 'adventurer',
                            name: line.split('-')[1].trim(),
                            pos: line.split('-')[2].trim() + '-' + line.split('-')[3].trim(),
                            orientation: line.split('-')[4].trim().toUpperCase(),
                            moves: line.split('-')[5].trim().toUpperCase(),
                            treasureCount: treasureCount,
                            isOnTreasure: isOnTreasure,
                        }
                    //On l'ajoute également au tableau des aventuriers
                    entryfileOject.adventurers.push(
                        {
                            name: line.split('-')[1].trim(),
                            pos: line.split('-')[2].trim() + '-' + line.split('-')[3].trim(),
                            orientation: line.split('-')[4].trim().toUpperCase(),
                            moves: line.split('-')[5].trim().toUpperCase(),
                            treasureCount: treasureCount,
                            isOnTreasure: isOnTreasure,
                        }
                    )
                    //Si l'aventurier est sur un trésor, on stocke les informations du trésor afin de pouvoir le replacer quend il se déplacera
                    if(isOnTreasure){
                        entryfileOject.mapInfo[line.split('-')[3].trim()][line.split('-')[2].trim()].treasureInfo = treasureInfo;
                    }
                    break;
                default:
                    break;
            }
        })

        return entryfileOject
    }

    return(
        <div className="page-container">
            <div className="main-content">
                <h1 className="text">La carte aux trésors</h1>
                {
                    errorMessage!==""? <div data-testid="errorMessageLbl" id='errorMessageLbl' className="text error">
                                            <p>Erreur dans le fichier d'entrée :</p> 
                                            {
                                                errorMessage.split('\n').map((line, index) =>{
                                                    return<p key={"error-line-"+index}>{line}</p>
                                                })
                                            } 
                                        </div> : ""
                }
                <p data-testid="entryFileLbl" id='entryFileLbl' className="text">Fichier d'entrée :</p>
                <textarea data-testid="entryFileText" id='entryFileText' className='entry-file-area' defaultValue={""} onChange={event => setEntryFile(event.target.value)}/>
                <br/>
                <button data-testid="startBtn" id='startBtn' onClick={startGame} className="btn">Commencer l'exploration</button>
            </div>
        </div>
    );
}

export default Home;